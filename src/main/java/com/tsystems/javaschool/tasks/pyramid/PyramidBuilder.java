package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        if (inputNumbers.size() == 1) {
            int[][] array = new int[1][];
            array[0][0] = inputNumbers.get(0);
            return array;
        }

        final int discriminant = 1 + 4 * inputNumbers.size() * 2;
        final boolean isTryanglePyramid = discriminant > 0;
        if (!isTryanglePyramid) {
            throw new CannotBuildPyramidException();
        }

        double trianglePositionNumber =  (-1 + Math.sqrt(discriminant))/2;
        int n = (int) Math.round(trianglePositionNumber);

        final boolean isn = Math.abs(trianglePositionNumber - n) > 0;
        if (isn) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int weight = n + n - 1;

        int[][] result = new int[n][weight];

        int start = 0;
        int size;
        for (int i = 0; i < n; i++) {
            size = i + 1;
            result[i] = generateRow(inputNumbers.subList(start, start + size), weight);
            start = start + size;
            System.out.println();
        }
        return result;
    }


    private int[] generateRow(List<Integer> list, int weight) {
        int[] resultRow = new int[weight];
        int n = list.size();
        int rows = (weight + 1) / 2;
        int skip = rows - list.size();
        int counter = 0;
        if (rows % 2 == 0) {
            for (int i = skip; i < weight; i++) {
                if (n % 2 == 0 && i % 2 == 0) {
                    resultRow[i] = list.get(counter++);
                    if (counter >= list.size()) {
                        break;
                    }
                } else if (n % 2 == 1 && i % 2 == 1) {
                    resultRow[i] = list.get(counter++);
                    if (counter >= list.size()) {
                        break;
                    }
                }
            }
        } else if (rows % 2 == 1) {
            for (int i = skip; i < weight; i++) {
                if (n % 2 == 1 && i % 2 == 0) {
                    resultRow[i] = list.get(counter++);
                    if (counter >= list.size()) {
                        break;
                    }
                } else if (n % 2 == 0 && i % 2 == 1) {
                    resultRow[i] = list.get(counter++);
                    if (counter >= list.size()) {
                        break;
                    }
                }
            }
        }

        return resultRow;
    }


}
