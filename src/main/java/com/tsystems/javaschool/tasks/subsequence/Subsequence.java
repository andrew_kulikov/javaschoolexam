package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        boolean result;
        if (x.size() > y.size()) {
            result = false;
            return result;
        }
        if (x.isEmpty() && y.isEmpty()) {
            result = true;
            return result;
        }

        if (y.containsAll(x)) {
            List duplicate = new ArrayList(y);
            duplicate.removeAll(x);
            y.removeAll(duplicate);
            final Set res = new TreeSet(y);
            int i = 0;
            for (Object st : res) {
                if (!x.get(i).equals(st)) {
                    result = false;
                    return result;
                }
                i++;
            }
            return true;
        } else {
            return false;
        }

    }
}
