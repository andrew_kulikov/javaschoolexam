package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
//   * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private enum Operation {
        ADDITION(0) {
            @Override
            protected Number execute(double o1, double o2) {
                return o1 + o2;
            }
        },
        SUBTRACTION(0) {
            @Override
            protected Number execute(double o1, double o2) {
                return o1 - o2;
            }
        },
        MULTIPLICATION(1) {
            @Override
            protected Number execute(double o1, double o2) {
                return o1 * o2;
            }
        },
        DIVISION(1) {
            @Override
            protected Number execute(double o1, double o2) {
                return o1 / o2;
            }
        },
        LEFT_BRACKET(2) {
            @Override
            protected Number execute(double o1, double o2) {
                return null;
            }
        },
        RIGHT_BRACKET(2) {
            @Override
            protected Number execute(double o1, double o2) {
                return null;
            }
        };

        public static Operation getByName(char name) {
            Operation result = null;
            switch (name) {
                case '+':
                    result = Operation.ADDITION;
                    break;
                case '-':
                    result = Operation.SUBTRACTION;
                    break;
                case '*':
                    result = Operation.MULTIPLICATION;
                    break;
                case '/':
                    result = Operation.DIVISION;
                    break;
                case '(':
                    result = Operation.LEFT_BRACKET;
                    break;
                case ')':
                    result = Operation.RIGHT_BRACKET;
                    break;
            }
            return result;
        }

        private final int priority;

        Operation(int priority) {
            this.priority = priority;
        }

        protected abstract Number execute(double o1, double o2);

        public Number run(Deque<Number> deque) {
            Number secondValue = deque.pollLast();
            if (secondValue == null) {
                return null;
            }

            Number firstValue = deque.pollLast();
            if (firstValue == null) {
                return null;
            }

            return execute(firstValue.doubleValue(), secondValue.doubleValue());
        }

        public int comparePriority(Operation o) {
            int result = 0;
            if (priority < o.priority) {
                result = -1;
            } else {
                if (priority > o.priority) {
                    result = 1;
                }
            }
            return result;
        }
    }


    private final Deque<Number> numbers = new ArrayDeque<Number>();
    private final Deque<Operation> operations = new ArrayDeque<Operation>();


    private boolean isSign(char value) {
        return value == '+' || value == '-';
    }

    private boolean isSeparator(char value) {
        return value == '.' || value == ',';
    }

    private int findNumber(int startIndex, String statement) {
        int indexInStatement = startIndex;
        if (indexInStatement >= statement.length()
//                || isSign(statement.charAt(indexInStatement))
        ) {
            indexInStatement++;
        }

        if (indexInStatement >= statement.length() || !Character.isDigit(statement.charAt(indexInStatement))) {
            return -1;
        }

        indexInStatement++;
        while (indexInStatement < statement.length() && Character.isDigit(statement.charAt(indexInStatement))) {
            indexInStatement++;
        }

        if (indexInStatement < statement.length() && isSeparator(statement.charAt(indexInStatement))) {
            indexInStatement++;

            if (indexInStatement >= statement.length() || !Character.isDigit(statement.charAt(indexInStatement))) {
                return indexInStatement - startIndex;
            }

            indexInStatement++;
            while (indexInStatement < statement.length() && Character.isDigit(statement.charAt(indexInStatement))) {
                indexInStatement++;
            }
        }
        return indexInStatement - startIndex;
    }

    private boolean processOperation(Operation operation) {
        Operation operationOnTop = null;
        if (operation == Operation.LEFT_BRACKET) {
            operations.add(operation);
        } else {
            if (operation == Operation.RIGHT_BRACKET) {
                operationOnTop = operations.peekLast();
                while (operationOnTop != null && operationOnTop != Operation.LEFT_BRACKET) {
                    operations.pollLast();
                    Number resultNumber = operationOnTop.run(numbers);
                    if(resultNumber == null) {
                        return false;
                    }
                    numbers.add(resultNumber);
                    operationOnTop = operations.peekLast();
                }
                operations.pollLast();
            } else {
                operationOnTop = operations.peekLast();
                while (operationOnTop != null && operation.priority <= operationOnTop.priority && operationOnTop != Operation.LEFT_BRACKET) {
                    operations.pollLast();
                    Number resultNumber = operationOnTop.run(numbers);
                    if(resultNumber == null) {
                        return false;
                    }
                    numbers.add(resultNumber);
                    operationOnTop = operations.peekLast();
                }
                operations.add(operation);
            }
        }
        return true;
    }

    private void clearStacks() {
        numbers.clear();
        operations.clear();
    }

    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }
        int numberLength = 0;
        Operation operation = null;
        int scanPosition = 0;
        boolean isPreviousNumber = false;

        while (scanPosition < statement.length()) {
            while (Character.isWhitespace(statement.charAt(scanPosition))) {
                scanPosition++;
            }
            if (isPreviousNumber) {
                operation = Operation.getByName(statement.charAt(scanPosition));
                if (operation != null) {
                    if (!processOperation(operation)) {
                        clearStacks();
                        return null;
                    }
                    scanPosition++;
                    if (operation != Operation.RIGHT_BRACKET) {
                        isPreviousNumber = false;
                    }
                } else {
                    clearStacks();
                    return null;
                }
            } else {
                numberLength = findNumber(scanPosition, statement);
                if (numberLength > 0) {
                    String number = statement.substring(scanPosition, scanPosition + numberLength);
                    Double value;
                    try {
                        value = Double.parseDouble(number);
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    numbers.add(value);
                    scanPosition += numberLength;
                    isPreviousNumber = true;
                } else {
                    operation = Operation.getByName(statement.charAt(scanPosition));
                    if (operation != null) {
                        if (!processOperation(operation)) {
                            clearStacks();
                            return null;
                        }
                        scanPosition++;
                    }
                }

            }
        }

        operation = operations.peekLast();
        while (operation != null) {
            if(operation == Operation.LEFT_BRACKET || operation == Operation.RIGHT_BRACKET) {
                //скобки не согласованы, к этому моменту скобок в стеке не должно быть
                break;
            }
            operations.pollLast();
            Number resultNumber = operation.run(numbers);
            if (resultNumber != null) {
                numbers.add(resultNumber);
            } else {
                clearStacks();
                return null;
            }

            operation = operations.peekLast();
        }

        //если стек операций не пуст или в стеке чисел больше чем одно число(результат),
        //то в выражении есть проблемы с согласованностью скобок
        if (numbers.size() > 1 || !operations.isEmpty()) {
            clearStacks();
            return null;
        }

        Number result = numbers.pollLast();
        DecimalFormat formatter = new DecimalFormat("#0.####", new DecimalFormatSymbols(Locale.US));
        if (result.doubleValue() == Double.NEGATIVE_INFINITY ||  result.doubleValue() == Double.POSITIVE_INFINITY) {
            return null;
        }
        return formatter.format(result.doubleValue());
    }

}
